import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

//Change Notifier
class NumberNotifier extends ChangeNotifier {
  int num = 0;

  void sumar() {
    num++;
    notifyListeners();
  }

  void restar() {
    num = max(0, num - 1);
    notifyListeners();
  }
}

final numberNotifierProvider =
    ChangeNotifierProvider<NumberNotifier>((ref) => NumberNotifier());

// State Notifier
class Number {
  final int num;

  Number({this.num = 0});

  Number copy({int? num}) => Number(num: num ?? this.num);
}

class MathNotifier extends StateNotifier<Number> {
  MathNotifier() : super(Number());

  void sumar() {
    final num = state.num + 1;
    final newState = state.copy(num: num);
    state = newState;
  }

  void restar() {
    final num = max(0, state.num - 1);
    final newState = state.copy(num: num);
    state = newState;
  }
}

final stateNotifierProvider = StateNotifierProvider<MathNotifier, Number>((ref) => MathNotifier());
