import 'package:flutter_riverpod/flutter_riverpod.dart';

// Provider
final numberProvider = Provider<int>((ref) => 42);

//State Provider
final stateProvider = StateProvider<int>((ref) => 0);

//Future Provider
Future<int> _fetchNumber() async {
  await Future.delayed(Duration(seconds: 2));
  return 20;
}

final futureProvider = FutureProvider<int>((ref) => _fetchNumber());

//Stream Provider
final streamProvider = StreamProvider.autoDispose<String>(
    (ref) => Stream.periodic(Duration(milliseconds: 500), (count) => '$count'));

//Combined Provider
final animalProvider = Provider<String>((ref) => 'Perro2');

Future<int> _fetchAnimalNumber(String animal) async {
  await Future.delayed(Duration(seconds: 2));

  return animal == 'Perro' ? 1 : 2;
}

final animalFutureProvider = FutureProvider<int>((ref) {
  final animal = ref.watch(animalProvider);
  return _fetchAnimalNumber(animal);
});
