


import 'package:ex5riverpod/pages/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';



void main() {
  runApp(ProviderScope(child: MyApp()));
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // bottomNavigationBarTheme: BottomNavigationBarThemeData(
        //   backgroundColor: Colors.amber,
        //   selectedItemColor: Colors.black,
        //   unselectedItemColor: Colors.white 
        // )
      ),
      title: 'Riverpod App',
      home: HomePage(),
    );
  }
}