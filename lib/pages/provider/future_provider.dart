import 'package:ex5riverpod/providers/providers_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class FutureProviderPage extends ConsumerWidget {
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final futureNumber = ref.watch(futureProvider);
    return Scaffold(
      appBar: AppBar(
        title: Text('Future Provider Page'),
      ),
      body: Center(
        child: futureNumber.when(
          data: (value) => Text('$value'),
          loading: () => CircularProgressIndicator(),
          error: (error, stackTrace) => Text('Error: $error'),
        ),
      ),
    );
  }
}
