import 'package:ex5riverpod/providers/providers_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class StateProviderPage extends ConsumerWidget {
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final number = ref.watch(stateProvider);

    return Scaffold(
      appBar: AppBar(
        title: Text('State Provider Page'),
      ),
      body: Center(
        child: Text('$number'),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          final provider = ref.read(stateProvider.notifier);
          provider.state++;
        },
      ),
    );
  }
}
