
import 'package:ex5riverpod/providers/providers_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';



class ProviderPage extends ConsumerWidget {
  // int number = 0;

  @override
  Widget build(BuildContext context, ref) {
    final number = ref.watch(stateProvider);
    return Scaffold(
      appBar: AppBar(
        title: Text('Provider'),
      ),
      body: Center(
        child: _generateText(number),
      ),
    );
  }

  Widget _generateText(int number) {
    if (number > 10) {
      return Text('Ganaste');
    }

    return Text(number.toString());
  }



}
