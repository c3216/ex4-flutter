import 'package:ex5riverpod/providers/providers_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class CombinedProviderPage extends ConsumerWidget {
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final animalNumber = ref.watch(animalFutureProvider);
    return Scaffold(
      appBar: AppBar(
        title: Text('Combining Providers'),
      ),
      body: Center(
        child: animalNumber.when(
          data: (value) => Text('$value'),
          loading: () => LinearProgressIndicator(),
          error: (e, stack) => Text('Error: $e')
        ),
      ),
    );
  }
}
