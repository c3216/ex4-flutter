import 'package:ex5riverpod/providers/providers_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class StreamProviderPage extends ConsumerWidget {
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Stream Provider'),
      ),
      body: Center(
        child: _buildStream(ref),
      ),
    );
  }

  Widget _buildStream(WidgetRef ref) {
    final stream = ref.watch(streamProvider);
    return stream.when(
        data: (value) => Text('$value'),
        error: (e, stack) => Text('Error: $e'),
        loading: () => CircularProgressIndicator());
  }
}
