
import 'package:ex5riverpod/pages/notifier/change_notifier_page.dart';
import 'package:ex5riverpod/pages/notifier/state_notifier_page.dart';
import 'package:flutter/material.dart';

import 'provider/combined_provider.dart';
import 'provider/future_provider.dart';
import 'provider/provider_page.dart';
import 'provider/state_provider_page.dart';
import 'provider/stream_provider_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int index = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Provider Page', style: TextStyle(color: Colors.black)),
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Center(child: buildPages()),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: index,
        // items: [
        //   BottomNavigationBarItem(
        //       icon: Text('Riverpod'), activeIcon: Text('R', style: TextStyle(color: Theme.of(context).primaryColor),), label: 'Providers'),
        //   BottomNavigationBarItem(
        //       icon: Text('Riverpod'), label: 'Notifiers'),
        //   BottomNavigationBarItem(
        //       icon: Text('Riverpod'), label: 'Modifiers')
        // ],
        items: [
          BottomNavigationBarItem(
              icon: Text('Riverpod'), label: 'Providers'),
          BottomNavigationBarItem(
              icon: Text('Riverpod'), label: 'Notifiers'),
          BottomNavigationBarItem(
              icon: Text('Riverpod'), label: 'Modifiers')
        ],
        onTap: (index) {
          setState(() {
            this.index = index;
          });
        },
      ),
    );
  }

  Widget buildPages() {
    switch (index) {
      case 0:
        return buildProviders(context);
      case 1:
        return buildNotifiers(context);
      default:
        return Container();
    }
  }

  Widget buildProviders(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ElevatedButton(
          child: Text(
            'Provider',
          ),
          onPressed: () {
            navigateTo(context, ProviderPage());
          },
        ),
        ElevatedButton(
          child: Text('State Provider'),
          onPressed: () {
            navigateTo(context, StateProviderPage());
          }
        ),
        ElevatedButton(
          child: Text('Future Provider'),
          onPressed: () {
            navigateTo(context, FutureProviderPage());
          }
        ),
        ElevatedButton(
          child: Text('Stream Provider'),
          onPressed: () {
            navigateTo(context, StreamProviderPage());
          }
        ),
        ElevatedButton(
          child: Text('Combined Provider'),
          onPressed: () {
            navigateTo(context, CombinedProviderPage());
          }
        )
      ],
    );
  }


  Widget buildNotifiers(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        ElevatedButton(
          child: Text('Change Notifier'),
          onPressed: () {
            navigateTo(context, ChangeNotifierPage());
          },
        ),
        ElevatedButton(
          child: Text('State Notifier'),
          onPressed: () {
            navigateTo(context, StateNotifierPage());
          },
        )
      ],
    );
  }

  void navigateTo(BuildContext context, Widget page) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (BuildContext context) => page));
  }
}
