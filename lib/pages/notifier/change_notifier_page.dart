import 'package:ex5riverpod/providers/notifiers_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class ChangeNotifierPage extends ConsumerWidget {
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final numberClass = ref.watch(numberNotifierProvider);
    return Scaffold(
      appBar: AppBar(
        title: Text('Change Notifier'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('${numberClass.num}'),
            SizedBox(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                ElevatedButton(
                  child: Icon(Icons.add),
                  onPressed: () {
                    ref.read(numberNotifierProvider).sumar();
                  },
                ), 
                ElevatedButton(
                  child: Icon(Icons.minimize),
                  onPressed: () {
                    ref.read(numberNotifierProvider).restar();
                  },
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
