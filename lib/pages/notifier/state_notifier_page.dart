import 'package:ex5riverpod/providers/notifiers_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class StateNotifierPage extends ConsumerWidget {
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final math = ref.watch(stateNotifierProvider);
    final mathNotifier = ref.read(stateNotifierProvider.notifier);

    return Scaffold(
      appBar: AppBar(
        title: Text('State Notifier'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Numero: ${math.num}'),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                ElevatedButton(
                  child: Icon(Icons.add),
                  onPressed: () {
                    mathNotifier.sumar();
                  },
                ),
                ElevatedButton(
                  child: Icon(Icons.minimize),
                  onPressed: () {
                    mathNotifier.restar();
                  },
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}


